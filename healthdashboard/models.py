from django.db import models

class Patient(models.Model):
    patientID = models.AutoField(primary_key=True)
    patientName = models.CharField(max_length=100)
    patientAge = models.IntegerField()

    GENDER_CHOICES = [
        ('male', 'Male'),
        ('female', 'Female'),
        ('non_binary', 'Non Binary'),
    ]

    patientSex = models.CharField(max_length=10, choices=GENDER_CHOICES)

    def __str__(self):
        return self.patientName

class ECG(models.Model):
    ecgID = models.AutoField(primary_key=True)
    ecgDataArray = models.TextField(blank=True)
    timestamp = models.DateTimeField()
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    

class BloodPressure(models.Model):
    bloodPressureID = models.AutoField(primary_key=True)
    bloodPressureDataArray = models.TextField(blank=True)
    timestamp = models.DateTimeField()
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    

class HeartRate(models.Model):
    heartRateID = models.AutoField(primary_key=True)
    heartRateDataArray = models.TextField(blank=True)
    timestamp = models.DateTimeField()
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)

class EmergencyCriteria(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    ecg_threshold = models.IntegerField()
    blood_pressure_threshold = models.IntegerField()
    heart_rate_threshold = models.IntegerField()


class MedicalSummary(models.Model):
    patient = models.OneToOneField(Patient, on_delete=models.CASCADE)
    diagnosis = models.CharField(max_length=100)
    stage = models.CharField(max_length=50)
    comorbid_conditions = models.CharField(max_length=200)

    def __str__(self):
        return f"Medical Summary of {self.patient.patientName}"