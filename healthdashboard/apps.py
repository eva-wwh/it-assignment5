from django.apps import AppConfig


class HealthdashboardConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "healthdashboard"
