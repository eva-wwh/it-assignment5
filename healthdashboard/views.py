from django.shortcuts import render, get_object_or_404
from .models import Patient, ECG, BloodPressure, HeartRate, EmergencyCriteria, MedicalSummary
import random
from django.http import JsonResponse

def index(request):
    # Retrieve patient information from the database
    patients = Patient.objects.all()
    # Retrieve medical summary from the database
    
    return render(request, 'patient_home.html', {'patients': patients})

def patient_detail(request, patient_id):
    # Retrieve detailed information of the selected patient
    patient = get_object_or_404(Patient, pk=patient_id)
    medical_summary = MedicalSummary.objects.get(patient=patient)
    ecg_data = ECG.objects.filter(patient=patient)
    bp_data = BloodPressure.objects.filter(patient=patient)
    hr_data = HeartRate.objects.filter(patient=patient)
    
    print(medical_summary, ecg_data, bp_data, hr_data)
    heart_rate = random.randint(60, 100) 
    ecg_value = random.randint(50, 150)   # Generate a random ECG value between 50 and 150
    blood_pressure = f"{random.randint(90, 140)}/{random.randint(60, 90)}"
    return render(request, 'home.html', {'patient': patient, 'heart_rate': heart_rate, 'ecg_value': ecg_value, 'blood_pressure': blood_pressure})

def patient_profile(request, patient_id):
    patient = Patient.objects.get(id=patient_id)
    return render(request, 'patient_profile.html', {'patient': patient})

def ecg_data(request, patient_id):
    ecg_data = ECG.objects.filter(patient_id=patient_id)
    return render(request, 'ecg_data.html', {'ecg_data': ecg_data})

def blood_pressure_data(request, patient_id):
    bp_data = BloodPressure.objects.filter(patient_id=patient_id)
    return render(request, 'blood_pressure_data.html', {'bp_data': bp_data})

def heart_rate_data(request, patient_id):
    hr_data = HeartRate.objects.filter(patient_id=patient_id)
    return render(request, 'heart_rate_data.html', {'hr_data': hr_data})

def emergency_criteria(request, patient_id):
    criteria = EmergencyCriteria.objects.get(patient_id=patient_id)
    return render(request, 'emergency_criteria.html', {'criteria': criteria})