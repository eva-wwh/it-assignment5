from django.contrib import admin

from .models import Patient, ECG, BloodPressure, HeartRate, EmergencyCriteria, MedicalSummary

admin.site.register(Patient)
admin.site.register(ECG)
admin.site.register(BloodPressure)
admin.site.register(HeartRate)
admin.site.register(EmergencyCriteria)
admin.site.register(MedicalSummary)