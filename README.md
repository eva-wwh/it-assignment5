# Installation

- Create virtual environment `python -m venv env`
- Activate environment `env/Scripts/activate.bat`
- Install dependencies `pip install -r requirements.txt`
- Makemigrations `python manage.py makemigrations`
- Makemigrations `python manage.py makemigrations healthdashboard`
- Migrate `python manage.py migrate`
- Create superuser `python manage.py createsuperuser`
- Make sure to add some initial data to the patient,ecg,heart rate, blood pressure and medical summary tables for atleast one patient id by logging into the admin panel at localhost:8000/admin.
- Launch server `python manage.py runserver`